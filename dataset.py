import sqlite3, hashlib
from datetime import datetime, timedelta
import random
import os
 
numRecords = 10
dbFile = os.path.join(os.getcwd(), 'database.db')
tableName = 'data'
minTemp = 20.0
maxTemp = 30.0
startDate = '2024-03-17 00:00:00'
period = 300


conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    id INTEGER PRIMARY KEY,
    timestamp TEXT NOT NULL,
    temperature REAL NOT NULL,
    humidity REAL NOT NULL
);
"""
cursor.execute(createQuery)

conn.commit()
conn.close()

tableName = 'users'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    id INTEGER PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    api_key TEXT
);
"""
cursor.execute(createQuery)
conn.commit()
conn.close()


tableName = 'user_status'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS user_status
    (logged_in BOOLEAN, 
    current_user TEXT)
"""
cursor.execute(createQuery)

cursor.execute("INSERT INTO user_status (logged_in, current_user) VALUES (?, ?)", (False, "No user"))

conn.commit()
conn.close()


tableName = 'notif'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    notify BOOLEAN,
    temperature INTEGER
);
"""
cursor.execute(createQuery)
cursor.execute("INSERT INTO notif (notify, temperature) VALUES (?, ?)", (False, None))

conn.commit()
conn.close()
