from flask import Blueprint, jsonify, json, request, flash, session
from datetime import datetime
import sqlite3, hashlib
from serial_manager import ser
import logging


api_routes_bp = Blueprint('api_routes', __name__)

@api_routes_bp.route('/api/measurments', methods=['GET'])
def get_values():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data")
    measurmets = cursor.fetchall()
    conn.close()
    for i in range(len(measurmets)):
        measurmets[i] = {"id": measurmets[i][0], "timestamp": measurmets[i][1], "temperature": measurmets[i][2], "humidity": measurmets[i][3]}
    return jsonify(measurmets), 200

@api_routes_bp.route('/api/measurments/<int:measurment_id>', methods=['GET'])
def get_single_measurment(measurment_id):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data WHERE id=?", (measurment_id,))
    measurment = cursor.fetchone()
    conn.close()    
    if measurment:
        return jsonify(measurment), 200
    else:
        return jsonify({"message": "Record not found"}), 404

@api_routes_bp.route('/api/measurments/<int:number_of_deletions>', methods=['DELETE'])
def delete_record(number_of_deletions):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT COUNT(*) FROM data")
    total_rows = cursor.fetchone()[0]

    if number_of_deletions > 0 and number_of_deletions <= total_rows:
        cursor.execute("DELETE FROM data WHERE id IN (SELECT id FROM data ORDER BY id ASC LIMIT ?)", (number_of_deletions,))
        cursor.execute("UPDATE data SET id = id - ?", (number_of_deletions,))
    else:
        cursor.execute("DELETE FROM data")

    conn.commit()
    conn.close()
    return jsonify({"message": f"Deleted {number_of_deletions} value(s)"}), 200

@api_routes_bp.route('/api/users', methods=['GET'])
def get_creds():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users")
    users = cursor.fetchall()
    conn.close()
    for i in range(len(users)):
        users[i] = {"id": users[i][0], "username": users[i][1], "password": users[i][2]}
    return jsonify(users), 200

@api_routes_bp.route('/api/users/<int:user_id>', methods=['GET'])
def get_cred(user_id):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE id=?", (user_id,))
    user = cursor.fetchone()
    conn.close()    
    if user:
        return jsonify({"id": user[0], "timestamp": user[1], "temperature": user[2]}), 200
    else:
        return jsonify({"message": "Credentials not found"}), 404

@api_routes_bp.route('/api/users', methods=['POST'])
def add_user():
    from main import database
    new_user = request.get_json()
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=?", (new_user['username'],))
    existing_user = cursor.fetchone()
    if existing_user:
        conn.close()
        flash('Username already exists. Please choose another one.', 'error')
        return jsonify({"error": "Username already exists"}), 400
    else:
        new_user['password'] = hashlib.sha256(new_user['password'].encode()).hexdigest()
        cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (new_user['username'], new_user['password']))
        cursor.execute("UPDATE user_status SET logged_in = ?, current_user = ?", (True, new_user['username']))
        conn.commit()
        conn.close()
        session['username'] = new_user['username']  # Set the username in the session
        return jsonify({'username':new_user['username'], 'password':new_user['password']}), 201

@api_routes_bp.route('/api/setNotification', methods=['POST'])
def api_write_serial():
    from main import database
    data = request.get_json()
    temperature = data['temp']
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("UPDATE notif SET notify = ?, temperature = ?", (True, temperature))
    conn.commit()
    conn.close()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@api_routes_bp.route('/api/getApi', methods=['GET'])
def get_user_api():
    from main import database
    logging.info("Endpoint /api/getApi was called")
    logging.info(f"Session data: {session}")
    
    if 'username' not in session:
        logging.error("Username not found in session")
        return jsonify({'error': 'User not logged in'}), 401

    try:
        conn = sqlite3.connect(database)
        cursor = conn.cursor()
        cursor.execute("SELECT api_key FROM users WHERE username=?", (session['username'],))
        result = cursor.fetchone()
        if result is None:
            logging.error("No API key found for user")
            return jsonify({'error': 'API key not found'}), 404
        api_key = result[0]
        conn.close()
        print(session['username'])
        print(api_key)
        logging.info(f"API Key retrieved: {api_key}")
        
        return jsonify({'api_key': api_key}), 200
    except Exception as e:
        logging.error(f"Error retrieving API key: {e}")
        return jsonify({'error': 'An error occurred'}), 500


@api_routes_bp.route('/api/postApi', methods=['POST'])
def post_user_api():
    from main import database
    new_api = request.get_json()
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("UPDATE users SET api_key = ? WHERE username = ?", (new_api['api_key'], session['username']))
    cursor.execute("SELECT api_key FROM users WHERE username=?", (session['username'],))
    result = cursor.fetchone()
    api_key = result[0]
    print(api_key)
    conn.commit()
    conn.close()
    return jsonify({'message': 'API key updated successfully'}), 200