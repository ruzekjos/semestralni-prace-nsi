from flask import Blueprint, render_template, request, redirect, flash, session
import sqlite3, hashlib
web_routes_bp = Blueprint('web_routes', __name__)

@web_routes_bp.route('/')
def welcome():
    return render_template('welcome.html')

@web_routes_bp.route('/login')
def login():
    return render_template('login.html')

@web_routes_bp.route('/register')
def register():
    return render_template('register.html')

@web_routes_bp.route('/dashboard')
def dashboard():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data")
    data = cursor.fetchall()
    measured_values = []
    for i in range(len(data)):
        measured_values.append({"id": data[i][0], "timestamp": data[i][1], "temperature": data[i][2], "humidity": data[i][3]})
    cursor.execute("SELECT logged_in FROM user_status")
    logged_in = cursor.fetchone()[0]
    cursor.execute("SELECT current_user FROM user_status")
    current_user = cursor.fetchone()[0]
    conn.close()
    return render_template('dashboard.html',logged_in=logged_in, measured_values=measured_values, current_user = current_user)

@web_routes_bp.route('/login-submit', methods=['POST'])
def login_submit():

    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=? AND password=?", (request.form['username'], hashlib.sha256(request.form['password'].encode()).hexdigest()))
    user = cursor.fetchone()
    if user and user[2] == hashlib.sha256(request.form['password'].encode()).hexdigest():
        flash('You have successfully logged in.', 'success')
        cursor.execute("UPDATE user_status SET logged_in = ?, current_user = ?", (True, request.form['username']))
        conn.commit()
        conn.close()

        session['username'] = request.form['username']  # Set the username in the session

        return redirect('/dashboard')
    else:
        flash('Invalid username or password, try a different one.', 'error')
        return redirect('/login')
    
@web_routes_bp.route('/apiInstructions')
def apiInstructions():
    return render_template('apiInstructions.html')