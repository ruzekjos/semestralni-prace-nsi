from flask import Flask, json, request, render_template, session
from routes import register_routes
import os
import secrets
import hashlib
import serial
import datetime
import sqlite3
import threading
import time
import paho.mqtt.client as mqtt
from serial_manager import init_serial, ser, lock, is_serial_initialized
from pushbullet import Pushbullet
from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)
register_routes(app)
app.secret_key = secrets.token_hex(16)

# Database path
database = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'database.db')

# MQTT configuration
MQTT_BROKER = 'test.mosquitto.org'
MQTT_PORT = 1883
MQTT_TOPIC = 'sensor/data'

def init_notif_table():
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("UPDATE notif SET notify = ?", (False,))
    conn.commit()
    conn.close()
init_notif_table()

# MQTT callback
def on_message(client, userdata, message):
    #print("Received payload:", message.payload.decode())
    data = json.loads(message.payload.decode())
    if 'timestamp' in data and 'temperature' in data and 'humidity' in data:
        timestamp = data['timestamp']
        temperature = data['temperature']
        humidity = data['humidity']
        conn = sqlite3.connect(database)
        cursor = conn.cursor()
        cursor.execute('INSERT INTO data (timestamp, temperature, humidity) VALUES (?, ?, ?)', (timestamp, temperature, humidity))

        # Select from the table
        cursor.execute('SELECT notify FROM notif')
        notify = cursor.fetchone()
        print(f"Result: {notify}")
        notify = notify[0] if notify else False
        if notify:  
            cursor.execute("SELECT temperature FROM notif")
            notif_temperature = cursor.fetchone()[0]
            cursor.execute("SELECT api_key FROM users")
            API_KEY = cursor.fetchone()[0]

            # Inicializace Pushbullet
            pb = Pushbullet(API_KEY)

            if temperature > notif_temperature:
                title = "Hodnota detekována"
                body = f"Současná teplota je {temperature}."
                push = pb.push_note(title, body)
                print(f"Push notification sent: {push}")
                cursor.execute("UPDATE notif SET notify = ?", (False,))


        conn.commit()
        conn.close()

client = mqtt.Client()
client.on_message = on_message
client.connect(MQTT_BROKER)
client.subscribe(MQTT_TOPIC, qos=0)
client.loop_start()

# Ensure the database and tables exist
def init_db():
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS data (
            id INTEGER PRIMARY KEY,
            timestamp TEXT NOT NULL,
            temperature REAL NOT NULL,
            humidity REAl NOT NULL
        )
    ''')
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            api_key TEXT
        )
    ''')
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS notif (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        notify BOOLEAN DEFAULT 1,
        temperature INTEGER NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

# Initialize the database
init_db()

def create_app():   
    return app

if __name__ == '__main__':
    try:
        app = create_app()
        app.run(host='0.0.0.0', port=3000, debug=True, use_reloader=False)
    except Exception as e:
        print(f"Failed to start the application: {e}")

