import network
import time
import ubinascii
import random
import json
from umqtt.simple import MQTTClient
from machine import Pin, I2C
import utime
import sys
import select

# Replace with your Wi-Fi credentials and MQTT broker details
SSID = 'Zyxel_E341_EXT'
PASSWORD ='H78LCU7A7BK7D478'
MQTT_BROKER = 'test.mosquitto.org'
MQTT_TOPIC = 'sensor/data'
PORT = 1883

led = Pin('LED', Pin.OUT)

# Simulate temperature readings
def simul_temp():
    return random.uniform(15, 25)

# Connect to Wi-Fi
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(SSID, PASSWORD)
    while not wlan.isconnected():
        print('Connecting to network...')  # UART communication
        time.sleep(1)
    print('Connected to', SSID)  # UART communication
    print('IP Address:', wlan.ifconfig()[0])  # UART communication

sleep_time = 10
MQTT = False

i2c = I2C(0, freq=100000)  # Using default pins for SDA and SCL with 100kHz frequency
# SHT30 default I2C address
SHT30_ADDR = 0x44

# Command to start periodic measurements
SHT30_MEAS_HIGHREP_STRETCH = b'\x2C\x06'

def read_sht30():
    # Send measurement command
    time.sleep(0.5)
    i2c.writeto(SHT30_ADDR, SHT30_MEAS_HIGHREP_STRETCH)
    time.sleep(0.5)
    
    # Read 6 bytes of data
    data = i2c.readfrom(SHT30_ADDR, 6)
    
    # Convert the data
    temp_raw = data[0] << 8 | data[1]
    temp = -45 + (175 * temp_raw / 65535.0)
    
    hum_raw = data[3] << 8 | data[4]
    humidity = 100 * (hum_raw / 65535.0)
    
    return temp, humidity
                
# Publish data to MQTT
def publish_data():

        global sleep_time 
        global MQTT 
        client_id = ubinascii.hexlify(network.WLAN().config('mac'), ':').decode()
        client = MQTTClient(client_id="RaspberryPiPico", server=MQTT_BROKER, port=PORT)
        client.connect()
        print('Connected to MQTT Broker')  # UART communication

        while True:
            
#vyber: senzor/nahodna hodnota
            temperature = simul_temp()
            humidity = simul_temp()

            # Main loop to read and print temperature and humidity

            #temperature, humidity = read_sht30()
            #time.sleep(2)

            # Blink LED to indicate activity
            led.value(1)
            time.sleep(0.5)
            led.value(0)
            
            timestamp = utime.localtime()
            formatted_timestamp = "{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}".format(timestamp[0], timestamp[1], timestamp[2], timestamp[3], timestamp[4], timestamp[5])

            data = {
                "timestamp": formatted_timestamp,
                "temperature": temperature,
                "humidity": humidity
            }
            message = json.dumps(data)  # Convert the dictionary to a JSON string

            client.publish(MQTT_TOPIC, message)
            print(f'Published message [{MQTT_TOPIC}]: {message}') # UART communication
            time.sleep(sleep_time)

# Main execution
connect_to_wifi()
publish_data()

